﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.ExceptionServices;
using UnityEngine;
using System.IO;

public class SaveLoad : MonoBehaviour
{
    [SerializeField]
    private StatSystem stats;

    //Player Data
    [SerializeField]
    private GameObject Player;

    private float ScarePoints;
    private float[] PlayerPosition;
    private GameManager.GameMode mode;

    //Boss Data
    [SerializeField]
    private GameObject Boss;

    private int BossHealth;
    private float[] BossPosition;

    //Enemy Data
    private float EnemyHealth;
    private float[] EnemyPostion;

    private string EmptyString;

    // Start is called before the first frame update
    void Start()
    {
        PlayerPosition = new float[3];
        BossPosition = new float[3];
        EnemyPostion = new float[3];
    }

    public void SaveGame()
    {
        //Setting Player Data
        mode = GameManager.Instance.CurrentMode;
        ScarePoints = ScarePointsBarUI.slider.value;
        PlayerPosition[0] = Player.transform.position.x;
        PlayerPosition[1] = Player.transform.position.y;
        PlayerPosition[2] = Player.transform.position.z;

        //Setting Boss Data
        BossPosition[0] = Boss.transform.position.x;
        BossPosition[1] = Boss.transform.position.y;
        BossPosition[2] = Boss.transform.position.z;
        
        BossHealth = (int)StatSystem.StatName.Health;

        //Setting Enemy Data

        //Save
        string SaveFile = Application.persistentDataPath + "/SaveData.txt";

        StreamWriter writer = new StreamWriter(SaveFile);

        writer.WriteLine("[Player Data]");
        writer.WriteLine("Mode = " + GameManager.Instance.CurrentMode);
        writer.WriteLine("Scare Points = " + ScarePoints);
        writer.WriteLine("Player X = " + PlayerPosition[0]);
        writer.WriteLine("Player Y = " + PlayerPosition[1]);
        writer.WriteLine("Player Z = " + PlayerPosition[2]);

        writer.WriteLine();
        writer.WriteLine("[Boss Data]");
        writer.WriteLine("Boss Health = " + BossHealth);
        writer.WriteLine("Boss X = " + BossPosition[0]);
        writer.WriteLine("Boss Y = " + BossPosition[1]);
        writer.WriteLine("Boss Z = " + BossPosition[2]);

        writer.Close();
    }

    public void LoadGame()
    {
        string SaveFile = Application.persistentDataPath + "/SaveData.txt";

        StreamReader reader = new StreamReader(SaveFile);

        while (reader.Peek() > -1)
        {
            string s = reader.ReadLine();
            if (s.Contains("Mode"))
            {
                if (s.Contains("StartMenu"))
                {
                    mode = GameManager.GameMode.StartMenu;
                }
                if (s.Contains("ScareTime"))
                {
                    mode = GameManager.GameMode.ScareTime;
                }
                if (s.Contains("BossFight"))
                {
                    mode = GameManager.GameMode.BossFight;
                }
                if (s.Contains("EndScreen"))
                {
                    mode = GameManager.GameMode.EndScreen;
                }
                continue;
            }
            if (s.Contains("Scare Points"))
            {
                GetValue(s);
                ScarePoints = float.Parse(EmptyString);
                continue;
            }
            if (s.Contains("Player X"))
            {
                GetValue(s);
                PlayerPosition[0] = float.Parse(EmptyString);
                continue;
            }
            if (s.Contains("Player Y"))
            {
                GetValue(s);
                PlayerPosition[1] = float.Parse(EmptyString);
                continue;
            }
            if (s.Contains("Player Z"))
            {
                GetValue(s);
                PlayerPosition[2] = float.Parse(EmptyString);
                continue;
            }

            if (s.Contains("Boss Health"))
            {
                GetValue(s);
                BossHealth = int.Parse(EmptyString);
                continue;
            }
            if (s.Contains("Boss X"))
            {
                GetValue(s);
                BossPosition[0] = float.Parse(EmptyString);
                continue;
            }
            if (s.Contains("Boss Y"))
            {
                GetValue(s);
                BossPosition[1] = float.Parse(EmptyString);
                continue;
            }
            if (s.Contains("Boss Z"))
            {
                GetValue(s);
                BossPosition[2] = float.Parse(EmptyString);
                continue;
            }

            if (s.Length == 0)
            {
                continue;
            }
        }

        reader.Close();

        //set player data
        GameManager.Instance.CurrentMode = mode;
        ScarePointsBarUI.slider.value = ScarePoints;
        Player.transform.position = new Vector3(PlayerPosition[0], PlayerPosition[1], PlayerPosition[2]);

        //set boss data
        stats.SetValue(StatSystem.StatName.Health, BossHealth);
        Boss.transform.position = new Vector3(BossPosition[0], BossPosition[1], BossPosition[2]);
    }

    public void GetValue(string text)
    {
        EmptyString = "";
        for (int i = 0; i < text.Length; i++)
        {
            if (char.IsDigit(text[i]))
            {
                EmptyString += text[i];
            }
            if (text[i] == '.')
            {
                EmptyString += text[i];
            }
        }
    }
}
