﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndScreen : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BackToMenu()
    {
        Time.timeScale = 1.0f;
        GameManager.Destroy(GameManager.Instance.gameObject);
        SceneManager.LoadScene(0);
    }
}
