﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScarePointsBarUI : MonoBehaviour
{
    [SerializeField]
    private GameObject EndScreenPanel;

    public static Slider slider;

    // Start is called before the first frame update
    void Start()
    {
        slider = GetComponent<Slider>();
    }

    // Update is called once per frame
    void Update()
    {
        slider.value -= Time.deltaTime;

        if (slider.value == 0)
        {
            Time.timeScale = 0;
            EndScreenPanel.SetActive(true);
        }
    }
}
