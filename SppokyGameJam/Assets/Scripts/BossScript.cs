﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Animations;
using UnityEngine.UI;

public class BossScript : MonoBehaviour
{
    public StatSystem stats;
    public List<Transform> points;
    public Transform Exit;
    public GameObject flashLight;
    public Image charSprite;
    public GameObject endScreen;
    private bool fleeing = false;
    private int currentPoint;
    private NavMeshAgent agent;
    private float baseSpeed;
    private float attackTimer;
    private Quaternion rotateTo;
    private bool alreadyRotating = true;
    private float lastX;

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.SetDestination(points[0].position);
        baseSpeed = agent.speed;
        attackTimer = Random.Range(5, 20);
        rotateTo = flashLight.transform.rotation;
        lastX = transform.position.x;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
        if (Vector3.Distance(transform.position, agent.destination) < 20) //If they get close to the point they start walking to a different one
        {
            ChangePoint();
            StartCoroutine("Sway", 180);
        }

        for(int i = -2; i <= 2; i++) //Math works but its super weird, will look this over later
        {
           RaycastHit hit;
           Debug.DrawRay(flashLight.transform.position, (flashLight.transform.forward + (flashLight.transform.right * (i * .2f))) * 90f, Color.red);
           if(Physics.Raycast(flashLight.transform.position, (flashLight.transform.forward + (flashLight.transform.right * (i * .2f))), out hit, 90f))
            {
                if (hit.transform.tag == "Player")
                {
                    Debug.Log("Hit the player end the game");
                    GameManager.Instance.CurrentMode = GameManager.GameMode.EndScreen;
                    endScreen.SetActive(true);
                    //Load end screen
                }
            }
        }

        attackTimer -= Time.deltaTime;
        if(attackTimer <= 0 && !alreadyRotating) //Every so often the investigator will start swinging his flashlight around
        {
            StartCoroutine("Sway", 90);
            attackTimer = Random.Range(5, 20);
        }
        
        flashLight.transform.rotation = Quaternion.Slerp(flashLight.transform.rotation, rotateTo, .01f);

        if (transform.position.x > lastX) //Flip the sprites depending on the direction it is walking in
        {
            charSprite.rectTransform.localRotation = new Quaternion(0, 180, 0, 0);
        }
        else
        {
            charSprite.rectTransform.localRotation = new Quaternion(0, 0, 0, 0);
        }
        lastX = transform.position.x;
    }

    private void ChangePoint()
    {

        if (currentPoint + 1 == points.Count)
        {
            currentPoint = 0;
        }
        else
        {
            currentPoint++;
        }
        agent.SetDestination(points[currentPoint].position);
        return;
    }

    public void TakeDamage(int damage)
    {
        Debug.Log("Boss hit");
        stats.SetValue(StatSystem.StatName.Health, stats.GetValue(StatSystem.StatName.Health) - damage);
        if (stats.GetValue(StatSystem.StatName.Health) == 0)
        {
            GameManager.Instance.CurrentMode = GameManager.GameMode.EndScreen; //Go to end screen

            return;
        }
        StartCoroutine("Spin");
        return;
    }

    IEnumerator Sway(int angle)
    {
        agent.speed = 0;
        alreadyRotating = true;

        rotateTo *= Quaternion.Euler(0, 90, 0);
        yield return new WaitForSeconds(2);
        rotateTo *= Quaternion.Euler(0, -angle, 0);
        agent.speed = baseSpeed;
        alreadyRotating = false;
    }

    IEnumerator Spin()
    {
        agent.speed = 0;
        float timer = 0;
        while (timer < 1)
        {
            Vector3 lookAt = Vector3.SlerpUnclamped(flashLight.transform.forward, -flashLight.transform.right, 5 * Time.deltaTime);
            flashLight.transform.rotation = Quaternion.LookRotation(lookAt);
            timer += Time.deltaTime;
            yield return null;
        }

        agent.speed = baseSpeed;
    }

}
