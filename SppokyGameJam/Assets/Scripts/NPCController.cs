﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class NPCController : MonoBehaviour
{
    public StatSystem stats;
    public List<Transform> points;
    public Transform Exit;
    public ParticleSystem tears;
    public Image charSprite;
    public Slider scarePointSlider;
    public GameObject investigator;
    private bool fleeing = false;
    private int currentPoint;
    private NavMeshAgent agent;
    private float lastX; //The last x position

    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.TotalNPCs += 1;
        agent = GetComponent<NavMeshAgent>();
        agent.SetDestination(points[0].position);
        lastX = transform.position.x;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(Vector3.Distance(transform.position, agent.destination) < 20) //If they get close to the point they start walking to a different one
        {
            ChangePoint();
        }

        if(transform.position.x > lastX) //Flip the sprites depending on the direction it is walking in
        {
            charSprite.rectTransform.localRotation = new Quaternion(0, 180, 0, 0);
        }
        else
        {
            charSprite.rectTransform.localRotation = new Quaternion(0, 0, 0, 0);
        }
        lastX = transform.position.x;
    }

    public void TakeDamage(int damage)
    {
        stats.SetValue(StatSystem.StatName.Health, stats.GetValue(StatSystem.StatName.Health) - damage);
        if(stats.GetValue(StatSystem.StatName.Health) == 0 || GameManager.Instance.CurrentMode == GameManager.GameMode.ScareTime)
        {
            tears.Play();
            agent.speed = 150;
            if(fleeing == false)
            {
                scarePointSlider.value += stats.GetValue(StatSystem.StatName.Exp);
                GameManager.Instance.ScareScore += stats.GetValue(StatSystem.StatName.Exp); //Change this to use the other scare points  
                fleeing = true;
                stats.SetValue(StatSystem.StatName.Running, 1);
                agent.SetDestination(Exit.position);
                GameManager.Instance.TotalNPCs -= 1;
            }
            
            if(GameManager.Instance.TotalNPCs == 0)
            {
                //Spawn the paranormal investigator
                investigator.SetActive(true);
                GameManager.Instance.CurrentMode = GameManager.GameMode.BossFight;
            }
            return;
        }
        return;
    }

    private void ChangePoint()
    {
        if(fleeing)
        {
            Destroy(gameObject); //When they leave the room they disapear
            return;
        }
        
        if(currentPoint+1 == points.Count)
        {
            currentPoint = 0;
        }
        else
        {
            currentPoint++;
        }
        agent.SetDestination(points[currentPoint].position);
        return;
    }

    public void EveryoneFlee() //Once enough people are scared everyone leaves the building at once and the paranormal investigator shows up
    {
        GameManager.Instance.ScareScore += stats.GetValue(StatSystem.StatName.Exp);
        GameManager.Instance.TotalScore += stats.GetValue(StatSystem.StatName.Exp);
        fleeing = true;
        stats.SetValue(StatSystem.StatName.Running, 1);
        agent.SetDestination(Exit.position);
        return;
    }
}
