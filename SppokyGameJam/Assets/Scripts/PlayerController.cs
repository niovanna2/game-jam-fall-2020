﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private Rigidbody rb;
    public float movementSpeed = 7;
    public float range = 5;
    public float moanCoolDownTime;
    public Slider scarePointSlider;
    public LayerMask mask;
    public Canvas charSprite;
    private float nextMoan;
    private Vector3 inputVector;
    public ParticleSystem moan;
    public GameObject endScreen;

    void Start()
    {
        
    }
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        Vector3 movementDirection = new Vector3(horizontal, 0.0f, vertical).normalized;
        if(movementDirection != Vector3.zero)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(movementDirection), 0.15F);
        }
        
        if (movementDirection.magnitude >= 0.1f)
        {
            inputVector = new Vector3(Input.GetAxisRaw("Horizontal"), 0.0f, Input.GetAxisRaw("Vertical"));
            rb.velocity = inputVector;
            rb.MovePosition(transform.position + (movementDirection * movementSpeed * Time.deltaTime));
        }
        

        charSprite.transform.rotation = Quaternion.Euler(90, 0, 0);

        

    }

    private void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            closestScare();
        }
        if (nextMoan < Time.time)
        {
            if (Input.GetButtonDown("Fire3"))
            {
                moanAttack();
                nextMoan = Time.time + moanCoolDownTime;

            }
        }

        if (transform.position.x < -850)
        {
            Time.timeScale = 0;
            endScreen.SetActive(true);
        }
    }
    private void OnDrawGizmosSelected() // makes range visible in editor (not play mode), range can be adjusted in editor
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
    void closestScare()
    {
        GameObject[] scares = GameObject.FindGameObjectsWithTag("Scares"); // finds scares
        float currentDistance = Mathf.Infinity;
        GameObject closestScareAttack = null;
        string name = " "; // = closestScareAttack.name;
        //print (name);
        foreach (GameObject scare in scares)
        {
            float distanceToScare = Vector3.Distance(transform.position, scare.transform.position);
            if (distanceToScare < currentDistance)
            {
                currentDistance = distanceToScare;
                closestScareAttack = scare;
                name = closestScareAttack.name;
            }
            if (closestScareAttack != null && currentDistance <= range) // make sure scares are in range
            {
                if (name != null)
                {
                    if (name == "Cup")
                    {
                        // move cup to new cup position
                        Vector3 newPosition = closestScareAttack.transform.position;
                        newPosition = new Vector3(newPosition.x, newPosition.y, newPosition.z - .5f); // fix this
                        closestScareAttack.transform.position = newPosition;
                        // subtract from enemy spook meter
                        scarePointSlider.value = scarePointSlider.value - 0.5f;
                        CheckForNPC(closestScareAttack.transform.position);
                    }
                    if (name == "Curtains")
                    {
                        // rustle curtains
                        closestScareAttack.transform.Rotate(0.0f, 0.0f, 45.0f);
                        // subtract from enemy spook meter
                        scarePointSlider.value = scarePointSlider.value - 0.5f;
                        CheckForNPC(closestScareAttack.transform.position);
                    }
                    if (name == "Door")
                    {
                        Debug.Log("dOOR");
                        // slam door
                        closestScareAttack.transform.Rotate(0.0f, 35.0f, 0.0f);
                        // subtract from enemy spook meter
                        scarePointSlider.value = scarePointSlider.value - 0.5f;
                        CheckForNPC(closestScareAttack.transform.position);
                    }
                    if (name == "Doll") //This is the powerup
                    {
                        scarePointSlider.value += 50;
                        Destroy(closestScareAttack);
                    }
                }
            }
        }
    }
    void moanAttack()
    {
        print("play");
        moan.Play(true);
        // make timer for particles
        // subtract from enemy spookmeter
        scarePointSlider.value -= 1; //Not sure if we want to make this worth more points
    }

    void CheckForNPC(Vector3 pos) //Check everything around the scare attack to see if there is an npc
    {
        RaycastHit[] hits = Physics.SphereCastAll(pos, 200, transform.forward, 0, mask, QueryTriggerInteraction.UseGlobal);

        if(hits != null)
        {
            foreach(RaycastHit hit in hits)
            {
                if(hit.collider.gameObject.GetComponent<NPCController>() != null)
                {
                    hit.collider.gameObject.GetComponent<NPCController>().TakeDamage(1);
                }
                else
                {
                    hit.collider.gameObject.GetComponent<BossScript>().TakeDamage(1);
                }
            }
        }
    }
}
