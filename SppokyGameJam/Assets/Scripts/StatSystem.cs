﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class StatSystem : MonoBehaviour
{
    public enum StatName
    {
        NULL = 0,
        Difficulty = 1,
        Speed = 2,
        AttackTime = 3,
        Running = 4,
        Health = 5,
        ScarePoints = 6,
        Exp = 7
    }
    [System.Serializable]
    public class Stat
    {
        public StatName name;
        public int value;
        public int maxValue;
    }

    [SerializeField] List<Stat> statList = new List<Stat>();

    public void SetValue(StatName statType, int newValue)
    {
        foreach(Stat s in statList)
        {
            if(s.name == statType)
            {
                if(newValue < 0)
                {
                    s.value = 0;
                    return;
                }
                else if(newValue > s.maxValue)
                {
                    s.value = s.maxValue;
                    return;
                }
                else
                {
                    s.value = newValue;
                    return;
                }
            }
        }
    }

    public int GetValue(StatName statType)
    {
        foreach(Stat s in statList)
        {
            if(s.name == statType)
            {
                return s.value;
            }
        }
        return int.MinValue;
    }

    public int GetMaxValue(StatName statType)
    {
        foreach (Stat s in statList)
        {
            if (s.name == statType)
            {
                return s.maxValue;
            }
        }
        return int.MinValue;
    }
}
