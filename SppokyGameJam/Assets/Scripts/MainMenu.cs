﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    private bool GameIsPaused;
    public void PlayGame()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("House");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    //Some input to call this function, whether it is by clicking a button or pressing a key.
    public void PauseGame()
    {
        if (GameIsPaused == false)
        {
            Time.timeScale = 0;
            GameIsPaused = true;
        }
        else
        {
            Time.timeScale = 1;
            GameIsPaused = false;
        }
    }
}
