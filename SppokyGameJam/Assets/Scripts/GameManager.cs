﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public enum GameMode
    {
        StartMenu = 1,
        ScareTime = 2,
        BossFight = 3,
        EndScreen = 4
    }

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    static private GameManager instance;
    static public GameManager Instance
    {
        get
        {
            if(instance == null)
            {
                instance = FindObjectOfType<GameManager>();
            }
            return instance;
        }
    }

    [SerializeField] private int totalScore=  0;
    public int TotalScore
    {
        get
        {
            return totalScore;
        }
        set
        {
            totalScore = value;

            if(totalScore >= 5)
            {
                Instance.CurrentMode = GameMode.BossFight;
                GameObject[] npcs = GameObject.FindGameObjectsWithTag("NPC");
                foreach(GameObject n in npcs)
                {
                    n.GetComponent<NPCController>().EveryoneFlee();
                }
                
            }
        }
    }

    [SerializeField]private int score = 0;
    public int ScareScore
    {
        get
        {
            return score;
        }
        set
        {
            score = value;
        }
    }

    [SerializeField]private GameMode currentMode;
    public GameMode CurrentMode
    {
        get
        {
            return currentMode;
        }
        set
        {
            currentMode = value;
        }
    }

    [SerializeField]private int totalNPCs = 0;
    public int TotalNPCs
    {
        get
        {
            return totalNPCs;
        }
        set
        {
            totalNPCs = value;
        }
    }


}
